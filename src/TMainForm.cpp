/*
 * TMainForm - main form (implementation)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fmx.h>
#pragma hdrstop

#include "TMainForm.h"

#include "TWebAuthForm.h"
#include "TBitbucket.h"
#include "CppUtils.h"
#include <client.h>
#include <FMX.Platform.hpp>
#include <System.IOUtils.hpp>

#if _WIN32
#include <tchar.h>
#include <FMX.Platform.Win.hpp>
#include <FMX.Dialogs.hpp>
#include <System.Win.Registry.hpp>
#endif /* _WIN32 */

namespace {
    /**
     * Returns the interface to a platform service.
     * This is a helper function for 'TPlatformService::GetPlatformService'.
     */
    template<typename I>
    DelphiInterface<I> GetPlatformService() {
        return TPlatformServices::Current->GetPlatformService(__uuidof(I));
    }
}

#pragma package(smart_init)
#pragma resource "*.fmx"
#pragma resource ("*.iPhone.fmx", _PLAT_IOS)

TMainForm *MainForm;

void __fastcall TMainForm::Log(const UnicodeString Message) {
    TVarRec params[] = {Message};
    Log(_D("%s"), params, 0);
}

void __fastcall TMainForm::Log(const UnicodeString Format, TVarRec *Params,
        const int Params_High) {
    auto logging = GetPlatformService<IFMXLoggingService>();
    if (logging) {
        logging->Log(Format, Params, Params_High);
    }
}

__fastcall TMainForm::TMainForm(TComponent *Owner)
        : TForm(Owner) {
    FCriticalSection = new TCriticalSection;

    Bitbucket = new TBitbucket(this);
    Bitbucket->OnRequestAuthorization = &BitbucketAuthenticate;
    Bitbucket->OnLoginComplete = &BitbucketLoginComplete;
    Bitbucket->Authenticator->ConsumerKey = _D("") CLIENT_ID;
    Bitbucket->Authenticator->ConsumerSecret = _D("") CLIENT_SECRET;
    Bitbucket->CallbackEndpoint = _D("") CALLBACK_URI;

#if _WIN32
    HRESULT result = RegisterApplicationRestart(CmdLine, 0);
    if (FAILED(result)) {
        Log(_D("RegisterApplicationRestart failed"));
    }
#endif
}

__fastcall TMainForm::~TMainForm() {
#if _WIN32
    HRESULT result = UnregisterApplicationRestart();
    if (FAILED(result)) {
        Log(_D("UnregisterApplicationRestart failed"));
    }
#endif

    Bitbucket = NULL;

    delete FUserIniFile;
    delete FCriticalSection;
}

TCustomIniFile *__fastcall TMainForm::GetUserIniFile() {
    // Avoids ambiguity with 'FMX.Objects.TPath'.
    using System::Ioutils::TPath;
    DelphiLock lock(FCriticalSection);
    if (!FUserIniFile) {
#if _WIN32
        FUserIniFile =
                new TRegistryIniFile
                (_D("SOFTWARE\\VX68K.org\\Bitbucket API Demo"));
#else
        UnicodeString name = TPath::GetCachePath();
        name = TPath::Combine(name, _D(".bitbucketview"));
        if (!TDirectory::Exists(name)) {
            TDirectory::CreateDirectory(name);
        }
        name = TPath::Combine(name, _D("user.ini"));
        FUserIniFile = new TMemIniFile(name);
#endif /* !_WIN32 */
    }
    return FUserIniFile;
}

void __fastcall TMainForm::WebAuthFormAuthorize(TObject *Sender,
        const UnicodeString VerificationCode) {
    if (!FAuthorized) {
        FAuthorized = true;
        Bitbucket->ContinueLogin(VerificationCode);
    }
}

void __fastcall TMainForm::WebAuthFormClose(TObject *Sender,
        TCloseAction &Action) {
#if !_WIN32
    if (!FAuthorized) {
        Bitbucket->Logout();
    }

    Action = TCloseAction::caFree;
    WebAuthForm = NULL;
#endif /* !_WIN32 */
}

void __fastcall TMainForm::FormShow(TObject *Sender) {
    if (Bitbucket->RestoreLoginState(UserIniFile)) {
        _di_IUserProfile profile = Bitbucket->CurrentUserProfile;
        if (profile) {
            AccountButton->Text = profile->Name;
        }
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserAccountUpdate(TObject *Sender) {
    UserAccount->Enabled = Bitbucket->Authenticated;
    AccountButton->Visible = Bitbucket->Authenticated;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserAccountExecute(TObject *Sender) {
    AccountPopup->IsOpen = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserLogoutExecute(TObject *Sender) {
    AccountPopup->IsOpen = false;
    Bitbucket->Logout();
    Bitbucket->SaveLoginState(UserIniFile);
    UserIniFile->UpdateFile();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserLoginUpdate(TObject *Sender) {
    UserLogin->Enabled =
            Bitbucket->LoginState == TBitbucket::TLoginState::LoggedOut;
    LoginButton->Visible = !Bitbucket->Authenticated;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UserLoginExecute(TObject *Sender) {
    Bitbucket->Login();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::BitbucketAuthenticate(TObject *Sender,
        const UnicodeString URL) {
    try {
        FAuthorized = false;
#if _WIN32
        SHELLEXECUTEINFO info = {};
        info.cbSize = sizeof(SHELLEXECUTEINFO);
        info.hwnd = Fmx::Platform::Win::FormToHWND(this);
        info.lpVerb = _T("open");
        info.lpFile = URL.c_str();
        info.nShow = SW_SHOWNORMAL;
        ShellExecuteEx(&info);

        UnicodeString verifier =
                InputBox(_D("Verification"), _D("&Verification code:"),
                _D("")).Trim();
        if (!verifier.IsEmpty()) {
            FAuthorized = true;
            Bitbucket->ContinueLogin(verifier);
        } else {
            // We assumes authentication was cancelled.
            Bitbucket->Logout();
        }
#else
        WebAuthForm = new TWebAuthForm(NULL);
        try {
            WebAuthForm->CallbackEndpoint = Bitbucket->CallbackEndpoint;
            WebAuthForm->OnAuthorize = WebAuthFormAuthorize;
            WebAuthForm->OnClose = &WebAuthFormClose;
            WebAuthForm->WebBrowser->URL = URL;
            WebAuthForm->Show();
        } catch (...) {
            delete WebAuthForm;
            throw;
        }
#endif /* !_WIN32 */
    } catch (...) {
        Bitbucket->Logout();
        throw;
    }
}

void __fastcall TMainForm::BitbucketLoginComplete(TObject *Sender) {
    Bitbucket->SaveLoginState(UserIniFile);
    UserIniFile->UpdateFile();

    _di_IUserProfile profile = Bitbucket->CurrentUserProfile;
    if (profile) {
        AccountButton->Text = profile->Name;
    }
}
