/*
 * CppUtils - utility classes for C++Builder (interface)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CppUtilsH
#define CppUtilsH

#include <System.SyncObjs.hpp>

namespace Cpputils {
    using namespace System::Syncobjs;

    template<typename Procedure, typename Function>
    class TFunctionProc : public TInterfacedObject, public Procedure {
    public:
        __fastcall TFunctionProc(Function Func)
                : FFunc(Func) {
        }

        void __fastcall operator()() {
            FFunc();
        }

        // TProc methods.
        virtual void __fastcall Invoke() {
            (*this)();
        }

        // IInterface methods.
        INTFOBJECT_IMPL_IUNKNOWN(TInterfacedObject);

    private:
        Function FFunc;
    };

    /*
     * Creates a TFunctionProc object for TProc.
     */
    template<typename Function>
    DelphiInterface<TProc> CreateProc(Function Func) {
        return new TFunctionProc<TProc, Function>(Func);
    }

    /*
     * Creates a TFunctionProc object for TThreadProcedure.
     */
    template<typename Function>
    DelphiInterface<TThreadProcedure> CreateThreadProcedure(Function Func) {
        return new TFunctionProc<TThreadProcedure, Function>(Func);
    }

    class DelphiLock {
    public:
        explicit __fastcall DelphiLock(TSynchroObject *Sync = NULL)
         : FSync(Sync) {
            if (FSync) {
                FSync->Acquire();
            }
        }

        __fastcall DelphiLock(DelphiLock && Lock)
                : FSync(Lock.FSync) {
            Lock.FSync = NULL;
        }

        __fastcall ~DelphiLock() {
            if (FSync) {
                FSync->Release();
            }
        }

        DelphiLock &operator=(DelphiLock &&Lock); // TODO: implement later.

        void __fastcall Reset(TSynchroObject *Sync = NULL) {
            if (Sync != FSync) {
                if (FSync) {
                    FSync->Release();
                }

                FSync = Sync;

                if (FSync) {
                    FSync->Acquire();
                }
            }
        }

    public:
        TSynchroObject *FSync;
    };
}
#ifndef NO_USING_NAMESPACE_CPPUTILS
using namespace Cpputils;
#endif

#endif
