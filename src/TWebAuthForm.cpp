/*
 * TAuthenticationForm - authentication form (implementation)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fmx.h>
#pragma hdrstop

#include "TWebAuthForm.h"

#if __APPLE__
#include <TargetConditionals.h>
#endif

#if (__APPLE__ && TARGET_OS_IPHONE) || __ANDROID__

static const UnicodeString OAuthVerifier = _D("oauth_verifier");

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
#pragma resource ("*.iPhone.fmx", _PLAT_IOS)

TWebAuthForm *WebAuthForm;
//---------------------------------------------------------------------------
__fastcall TWebAuthForm::TWebAuthForm(TComponent *Owner)
        : TForm(Owner) {
}
//---------------------------------------------------------------------------
void __fastcall TWebAuthForm::SetCallbackEndpoint
        (const UnicodeString CallbackEndpoint) {
    if (CallbackEndpoint == _D("oob")) {
        FCallbackEndpoint = UnicodeString();
    } else {
        FCallbackEndpoint = CallbackEndpoint;
    }
}

void __fastcall TWebAuthForm::SetOnAuthorize(TAuthorizeEvent Event) {
    FOnAuthorize = Event;
}

void __fastcall TWebAuthForm::DoAuthorize(
        const UnicodeString VerificationCode) {
    if (FOnAuthorize) {
        FOnAuthorize(this, VerificationCode);
    }
}

bool __fastcall TWebAuthForm::GetParamValue(const UnicodeString URL,
        const UnicodeString Name, UnicodeString &Value) {
     auto queryIndex = URL.Pos(_D("?"));
     if (queryIndex < 0) {
         return false;
     }
     queryIndex += 1;

     auto params = new TStringList();
     params->NameValueSeparator = _D('=');
     params->Delimiter = _D('&');
     params->DelimitedText =
             URL.SubString(queryIndex, URL.Length() - queryIndex);

     auto nameIndex = params->IndexOfName(Name);
     if (nameIndex >= 0) {
         Value = params->ValueFromIndex[nameIndex];
         return true;
     }
     return false;
}

void __fastcall TWebAuthForm::WebBrowserShouldStartLoadWithRequest
        (TObject *ASender, const UnicodeString URL) {
    TestStatusLabel->Text = _D("Would load ") + URL;
    if (FCallbackEndpoint.Length() != 0 && StartsStr(FCallbackEndpoint +
            _D("?"), URL)) {
        UnicodeString VerificationCode;
        if (GetParamValue(URL, OAuthVerifier, VerificationCode)) {
            DoAuthorize(VerificationCode);
        }
        Close();
    }
}

void __fastcall TWebAuthForm::WebBrowserDidFinishLoad(TObject *ASender)
{
    TestStatusLabel->Text = _D("Loaded ") + WebBrowser->URL;
    if (FCallbackEndpoint.Length() != 0 && StartsStr(CallbackEndpoint +
            _D("?"), WebBrowser->URL)) {
        UnicodeString VerificationCode;
        if (GetParamValue(WebBrowser->URL, OAuthVerifier, VerificationCode)) {
            DoAuthorize(VerificationCode);
        }
        Close();
    }
}

#endif
