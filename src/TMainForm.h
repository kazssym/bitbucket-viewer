/*
 * TMainForm - main form (interface)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TMainFormH
#define TMainFormH 1

#include <FMX.TabControl.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <REST.Authenticator.OAuth.hpp>
#include <REST.Client.hpp>
#include <IPPeerClient.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Data.Bind.Components.hpp>
#include <System.Actions.hpp>
#include <System.IniFiles.hpp>
#include <System.Classes.hpp>

class TMainForm : public TForm {
__published:
    TTabControl *TabControl1;
    TTabItem *TabItem1;
    TToolBar *ToolBar1;
    TSpeedButton *AccountButton;
    TPopup *AccountPopup;
    TPanel *AccountPopupPanel;
    TSpeedButton *LogoutButton;
    TSpeedButton *LoginButton;

    TStyleBook *StyleBook1;

    TActionList *ActionList1;
    TAction *UserAccount;
    TAction *UserLogout;
    TAction *UserLogin;
    TRESTClient *RESTClient1;
    TRESTResponse *RESTResponse1;
    TRESTRequest *RESTRequest1;

    void __fastcall FormShow(TObject *Sender);
    void __fastcall UserAccountUpdate(TObject *Sender);
    void __fastcall UserAccountExecute(TObject *Sender);
    void __fastcall UserLogoutExecute(TObject *Sender);
    void __fastcall UserLoginUpdate(TObject *Sender);
    void __fastcall UserLoginExecute(TObject *Sender);

    void __fastcall WebAuthFormAuthorize(TObject *Sender,
            const UnicodeString VerificationCode);
    void __fastcall WebAuthFormClose(TObject *Sender, TCloseAction &Action);

    void __fastcall BitbucketAuthenticate(TObject *Sender,
            const UnicodeString URL);
    void __fastcall BitbucketLoginComplete(TObject *Sender);

public:
    __fastcall TMainForm(TComponent *Owner);
    virtual __fastcall ~TMainForm();

protected:
    __property TCustomIniFile *UserIniFile = {read = GetUserIniFile};

    static void __fastcall Log(const UnicodeString Message);
    static void __fastcall Log(const UnicodeString Format, TVarRec *Params,
            const int Params_High);

    TCustomIniFile *__fastcall GetUserIniFile();

private:
    TCriticalSection *FCriticalSection;
    TCustomIniFile *FUserIniFile;

    bool FAuthorized;
};

extern PACKAGE TMainForm *MainForm;

#endif
