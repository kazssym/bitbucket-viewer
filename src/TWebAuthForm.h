/*
 * TAuthenticationForm - authentication form (interface)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWebAuthFormH
#define TWebAuthFormH 1

#if !_WIN32

#include <BitbucketAPI.hpp>
#include <FMX.StdActns.hpp>
#include <FMX.Forms.hpp>
#include <FMX.WebBrowser.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <System.Actions.hpp>
#include <System.Classes.hpp>

class TWebAuthForm : public TForm {
__published:
    TWebBrowser *WebBrowser;
    TToolBar *ToolBar1;
    TSpeedButton *CloseButton;
    TStatusBar *TestStatusBar;
    TLabel *TestStatusLabel;

    TActionList *ActionList1;
    TWindowClose *WindowClose;

    void __fastcall WebBrowserShouldStartLoadWithRequest(TObject *ASender,
            const UnicodeString URL);
    void __fastcall WebBrowserDidFinishLoad(TObject *ASender);

public:
    __property UnicodeString CallbackEndpoint = {
        read = FCallbackEndpoint, write = SetCallbackEndpoint};

    __property TAuthorizeEvent OnAuthorize = {
        read = FOnAuthorize, write = SetOnAuthorize
    };

    __fastcall TWebAuthForm(TComponent *Owner);

    void __fastcall SetCallbackEndpoint(const UnicodeString CallbackEndpoint);

protected:
    static bool __fastcall GetParamValue(const UnicodeString URL,
            const UnicodeString Name, UnicodeString &Value);

    void __fastcall SetOnAuthorize(TAuthorizeEvent Event);

    void __fastcall DoAuthorize(const UnicodeString VerificationCode);

private:
    UnicodeString FCallbackEndpoint;
    TAuthorizeEvent FOnAuthorize;
};
//---------------------------------------------------------------------------
extern PACKAGE TWebAuthForm *WebAuthForm;
//---------------------------------------------------------------------------

#endif /* !_WIN32 */

#endif
