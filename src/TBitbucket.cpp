/*
 * TBitbucket - support for Bitbucket REST API (interface)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fmx.h>
#pragma hdrstop

#include "TBitbucket.h"

#include "CppUtils.h"
#include <functional>
#include <memory>
#include <cassert>

#if __cplusplus < 201103L && _WIN32
#include <boost/shared_ptr.hpp>
using boost::shared_ptr;
#endif

#if __APPLE__ && __arm__
// The iOS SDK does not provide 'std::shared_ptr'.
#define shared_ptr auto_ptr
#endif

using namespace std;

namespace {
    /* Temporary solution for missing 'std::bind' for closures. */

    template<typename Function, typename T1>
    class unary_binder {
    public:
        unary_binder(Function func, T1 arg1)
                : _func(func),
                  _arg1(arg1) {
        }

        void operator()() {
            _func(_arg1);
        }

    private:
        Function _func;
        T1 _arg1;
    };

    template<typename Function, typename T1>
    unary_binder<Function, T1> bind1(Function Func, T1 Arg1) {
        return unary_binder<Function, T1>(Func, Arg1);
    }

    /**
     * Implementation of version 1.0 user profiles.
     */
    class TUserProfile1 : public TInterfacedObject, private IUserProfile {
        typedef TInterfacedObject inherited;

    public:
        __property UnicodeString Name = {read = FName, write = FName};
        __property UnicodeString FirstName = {
            read = FFirstName, write = FFirstName};
        __property UnicodeString LastName = {
            read = FLastName, write = FLastName};
        __property UnicodeString AvatarURI = {
            read = FAvatarURI, write = FAvatarURI};

        virtual UnicodeString __stdcall GetName() {
            return FName;
        }

        virtual UnicodeString __stdcall GetDisplayName() {
            return FFirstName + _D(" ") + FLastName;
        }

        virtual UnicodeString __stdcall GetAvatarURI() {
            return FAvatarURI;
        }

        INTFOBJECT_IMPL_IUNKNOWN(inherited);

    private:
        UnicodeString FName;
        UnicodeString FFirstName;
        UnicodeString FLastName;
        UnicodeString FAvatarURI;
    };
}

static const UnicodeString CredentialsSection = _D("Bitbucket Credentials");
static const UnicodeString CredentialsTokenID = _D("Token ID");
static const UnicodeString CredentialsTokenSecret = _D("Token Secret");

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"
TBitbucket *Bitbucket;
//---------------------------------------------------------------------------
__fastcall TBitbucket::TBitbucket(TComponent *Owner)
        : TDataModule(Owner) {
    FCriticalSection = new TCriticalSection();
}

__fastcall TBitbucket::~TBitbucket() {
    delete FCriticalSection;
}

UnicodeString __fastcall TBitbucket::GetCallbackEndpoint() {
    return FCallbackEndpoint;
}

void __fastcall TBitbucket::SetCallbackEndpoint(const UnicodeString URL) {
    FCallbackEndpoint = URL;
}

_di_IUserProfile __fastcall TBitbucket::GetCurrentUserProfile() {
    DelphiLock lock(FCriticalSection);
    if (!FCurrentUserProfile) {
        UserRequest->Execute();

        auto value = UserRequest->Response->JSONValue;
        auto object = dynamic_cast<TJSONObject *>(value);
        if (object) {
            auto user = object->GetValue(_D("user"));
            auto userObject = dynamic_cast<TJSONObject *>(user);
            if (userObject) {
                auto profile = new TUserProfile1();
                profile->GetInterface(FCurrentUserProfile);
                assert(FCurrentUserProfile);

                auto name = userObject->GetValue(_D("username"));
                auto firstName = userObject->GetValue(_D("first_name"));
                auto lastName = userObject->GetValue(_D("last_name"));
                auto avatarURI = userObject->GetValue(_D("avatar"));

                profile->Name = dynamic_cast<TJSONString *>(name)->Value();
                profile->FirstName = dynamic_cast<TJSONString *>(firstName)
                        ->Value();
                profile->LastName = dynamic_cast<TJSONString *>(lastName)
                        ->Value();
                profile->AvatarURI = dynamic_cast<TJSONString *>(avatarURI)
                        ->Value();
            }
        }
    }
    return FCurrentUserProfile;
}

TRequestAuthorizationEvent __fastcall TBitbucket::GetOnRequestAuthorization()
{
    return FOnRequestAuthorization;
}

void __fastcall TBitbucket::SetOnRequestAuthorization
        (TRequestAuthorizationEvent Event) {
    if (FOnRequestAuthorization != Event) {
        FOnRequestAuthorization = Event;
    }
}

TNotifyEvent __fastcall TBitbucket::GetOnLoginComplete() {
    return FOnLoginComplete;
}

void __fastcall TBitbucket::SetOnLoginComplete(TNotifyEvent Event) {
    if (FOnLoginComplete != Event) {
        FOnLoginComplete = Event;
    }
}

void __fastcall TBitbucket::RequestAuthorization(const UnicodeString URL) {
    if (FOnRequestAuthorization) {
        FOnRequestAuthorization(this, URL);
    }
}

void __fastcall TBitbucket::LoginComplete() {
    if (FOnLoginComplete) {
        FOnLoginComplete(this);
    }
}

void __fastcall TBitbucket::SaveLoginState(TCustomIniFile *IniFile) {
    if (Authenticator->AccessToken.Length() != 0) {
        IniFile->WriteString(CredentialsSection, CredentialsTokenID,
                Authenticator->AccessToken);
        IniFile->WriteString(CredentialsSection, CredentialsTokenSecret,
                Authenticator->AccessTokenSecret);
    } else {
        IniFile->DeleteKey(CredentialsSection, CredentialsTokenID);
        IniFile->DeleteKey(CredentialsSection, CredentialsTokenSecret);
    }
}

bool __fastcall TBitbucket::RestoreLoginState(TCustomIniFile *IniFile) {
    DelphiLock lock(FCriticalSection);
    if (FLoginState != TLoginState::LoggedOut) {
        return false;
    }

    auto ID = IniFile->ReadString(CredentialsSection, CredentialsTokenID,
            UnicodeString());
    auto Secret = IniFile->ReadString(CredentialsSection,
            CredentialsTokenSecret, UnicodeString());
    if (ID.Length() != 0 && Secret.Length() != 0) {
        Authenticator->AccessToken = ID;
        Authenticator->AccessTokenSecret = Secret;
        FLoginState = TLoginState::LoggedIn;
        return true;
    }
    return false;
}

void __fastcall TBitbucket::Login() {
    DelphiLock lock(FCriticalSection);
    if (LoginState != TLoginState::LoggedOut) {
        return;
    }
    FLoginState = TLoginState::ObtainingRequestToken;
    lock.Reset();

    TThread::CreateAnonymousThread(CreateProc(&ObtainRequestToken))->Start();
}

void __fastcall TBitbucket::ContinueLogin(const UnicodeString Verifier) {
    DelphiLock lock(FCriticalSection);
    if (LoginState != TLoginState::WaitingForAuthentication) {
        return;
    }
    FLoginState = TLoginState::ObtainingAccessToken;
    lock.Reset();

    Authenticator->VerifierPIN = Verifier;
    TThread::CreateAnonymousThread(CreateProc(&ObtainAccessToken))->Start();
}

void __fastcall TBitbucket::Logout() {
    DelphiLock lock(FCriticalSection);
    FLoginState = TLoginState::LoggedOut;
    Authenticator->AccessToken = _D("");
    Authenticator->AccessTokenSecret = _D("");
    lock.Reset();

    FCurrentUserProfile = NULL;
}

void __fastcall TBitbucket::ObtainRequestToken() {
    try {
        Authenticator->AccessToken = _D("");
        Authenticator->AccessTokenSecret = _D("");
        Authenticator->RequestToken = _D("");
        Authenticator->RequestTokenSecret = _D("");
        if (FCallbackEndpoint.Length() == 0) {
            Authenticator->CallbackEndpoint = _D("oob");
        } else {
            Authenticator->CallbackEndpoint = FCallbackEndpoint;
        }

        shared_ptr<TRESTClient> requestTokenClient(new TRESTClient(NULL));
        requestTokenClient->BaseURL = Authenticator->RequestTokenEndpoint;
        requestTokenClient->Authenticator = Authenticator;

        // This object will be owned.
        auto request = new TRESTRequest(requestTokenClient.get());
        request->Method = rmPOST;
        request->Accept = _D("application/x-www-form-urlencoded,*/*;q=0.1");
        request->Execute();

        Authenticator->CallbackEndpoint = _D(""); // No longer needed.

        UnicodeString ID, Secret;
        if (!GetCredentials(request->Response, ID, Secret)) {
            throw ERESTException(_D("Missing credentials in response"));
        }

        /* Note: we must somehow set the temporary credentials as the token
         * credentials to generate a signature right. */
        Authenticator->AccessToken = ID;
        Authenticator->AccessTokenSecret = Secret;
    } catch (ERESTException &e) {
        DelphiLock lock(FCriticalSection);
        FLoginState = TLoginState::LoggedOut;
        throw;
    } catch (...) {
        DelphiLock lock(FCriticalSection);
        FLoginState = TLoginState::LoggedOut;
        throw;
    }

    DelphiLock lock(FCriticalSection);
    if (LoginState != TLoginState::ObtainingRequestToken) {
        return;
    }
    FLoginState = TLoginState::WaitingForAuthentication;
    lock.Reset();

    UnicodeString url = Authenticator->AuthenticationEndpoint;
    url += _D("?oauth_token=") + URIEncode(Authenticator->AccessToken);
    TThread::Synchronize(TThread::CurrentThread,
            CreateThreadProcedure(bind1(&RequestAuthorization, url)));
}

void __fastcall TBitbucket::ObtainAccessToken() {
    try {
        shared_ptr<TRESTClient> accessTokenClient(new TRESTClient(NULL));
        accessTokenClient->BaseURL = Authenticator->AccessTokenEndpoint;
        accessTokenClient->Authenticator = Authenticator;

        // This object will be owned.
        auto request = new TRESTRequest(accessTokenClient.get());
        request->Method = rmPOST;
        request->Accept = _D("application/x-www-form-urlencoded,*/*;q=0.1");
        // Note: oauth_verifier is not included automatically.
        request->AddParameter(_D("oauth_verifier"),
                Authenticator->VerifierPIN);
        request->Execute();

        UnicodeString ID, Secret;
        if (!GetCredentials(request->Response, ID, Secret)) {
            throw ERESTException(_D("Missing credentials in response"));
        }

        Authenticator->AccessToken = ID;
        Authenticator->AccessTokenSecret = Secret;
    } catch (ERESTException &e) {
        DelphiLock lock(FCriticalSection);
        FLoginState = TLoginState::LoggedOut;
        throw;
    } catch (...) {
        DelphiLock lock(FCriticalSection);
        FLoginState = TLoginState::LoggedOut;
        throw;
    }

    DelphiLock lock(FCriticalSection);
    if (LoginState != TLoginState::ObtainingAccessToken) {
        return;
    }
    FLoginState = TLoginState::LoggedIn;
    lock.Reset();

    TThread::Synchronize(TThread::CurrentThread,
            CreateThreadProcedure(&LoginComplete));
}

bool __fastcall TBitbucket::GetCredentials(TCustomRESTResponse *Response,
        UnicodeString &ID, UnicodeString &Secret) {
    // Note: the REST client library appears to dislike content type
    // 'application/x-www-form-urlencoded'.
    if (SameText(Response->ContentType,
            CONTENTTYPE_APPLICATION_X_WWW_FORM_URLENCODED)) {
        Response->ContentType = CONTENTTYPE_TEXT_PLAIN;
    }

    return Response->GetSimpleValue(_D("oauth_token"), ID)
            && Response->GetSimpleValue(_D("oauth_token_secret"), Secret);
}
