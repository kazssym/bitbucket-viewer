/*
 * TBitbucket - support for Bitbucket REST API (interface)
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TBitbucketH
#define TBitbucketH 1

#include <BitbucketAPI.hpp>
#include <REST.Authenticator.OAuth.hpp>
#include <REST.Client.hpp>
#include <IPPeerClient.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Data.Bind.Components.hpp>
#include <System.IniFiles.hpp>
#include <System.Classes.hpp>

class TBitbucket : public TDataModule {
__published:
    TOAuth1Authenticator *Authenticator;
    TRESTClient *RESTClient;
    TRESTResponse *RESTResponse;

    TRESTRequest *UserRequest;
    TRESTRequest *UsersRequest;

public:
    enum class TLoginState : unsigned char {
        LoggedOut, ObtainingRequestToken, WaitingForAuthentication,
        ObtainingAccessToken, LoggedIn,
    };

    /**
     * OAuth 1.0 callback URI.
     * If this property is empty (default value), the actual value of the
     * 'oauth_callback' parameter SHALL be 'oob' (case-sensitive).
     */
    __property UnicodeString CallbackEndpoint = {
        read = GetCallbackEndpoint, write = SetCallbackEndpoint};

    /**
     * Returns true if authenticated with Bitbucket.
     */
    __property bool Authenticated = {read = GetAuthenticated};

    __property TLoginState LoginState = {read = FLoginState};

    /**
     * Profile of the current authenticated user.
     */
    __property _di_IUserProfile CurrentUserProfile = {
        read = GetCurrentUserProfile};

    __property TRequestAuthorizationEvent OnRequestAuthorization = {
        read = GetOnRequestAuthorization, write = SetOnRequestAuthorization};
    __property TNotifyEvent OnLoginComplete = {
        read = GetOnLoginComplete, write = SetOnLoginComplete};

    __fastcall TBitbucket(TComponent *Owner);
    virtual __fastcall ~TBitbucket();

    /**
     * Getter method for property 'IsAuthenticated'.
     */
    bool __fastcall GetAuthenticated() {
        return FLoginState == TLoginState::LoggedIn;
    }

    void __fastcall SaveLoginState(TCustomIniFile *IniFile);
    bool __fastcall RestoreLoginState(TCustomIniFile *IniFile);

    void __fastcall Login();
    void __fastcall ContinueLogin(const UnicodeString Verifier);

    /**
     * Logs out from the Bitbucket REST API.
     * The token credentials SHALL also be cleared.
     */
    void __fastcall Logout();

    /**
     * Returns the profile of the current authenticated user.
     */
    _di_IUserProfile __fastcall GetCurrentUserProfile();

    /**
     * Returns the profile of a user.
     */
    _di_IUserProfile2 __fastcall GetUserProfile(const UnicodeString UserName);

protected:
    static bool __fastcall GetCredentials(TCustomRESTResponse *Response,
            UnicodeString &ID, UnicodeString &Secret);

    UnicodeString __fastcall GetCallbackEndpoint();
    void __fastcall SetCallbackEndpoint(const UnicodeString URL);

    TRequestAuthorizationEvent __fastcall GetOnRequestAuthorization();
    void __fastcall SetOnRequestAuthorization
            (TRequestAuthorizationEvent Event);
    TNotifyEvent __fastcall GetOnLoginComplete();
    void __fastcall SetOnLoginComplete(TNotifyEvent Event);

    void __fastcall RequestAuthorization(const UnicodeString URL);
    void __fastcall LoginComplete();

    void __fastcall ObtainRequestToken();
    void __fastcall ObtainAccessToken();

private:
    TCriticalSection *FCriticalSection;
    UnicodeString FCallbackEndpoint;
    TLoginState FLoginState;
    _di_IUserProfile FCurrentUserProfile;
    TRequestAuthorizationEvent FOnRequestAuthorization;
    TNotifyEvent FOnLoginComplete;
};

extern PACKAGE TBitbucket *Bitbucket;

#endif
